import axios from "axios";
import store from '../store'

export default {
  methods: {
    jokeApi() {
      axios.get('https://icanhazdadjoke.com/', {headers: {'Accept': 'application/json'}})
      .then(({data: {joke}}) => {
        store.commit('setActualJoke', joke)
      })
    }
  }
}