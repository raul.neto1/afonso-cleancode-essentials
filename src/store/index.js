import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    actualJoke: null
  },
  mutations: {
    setActualJoke(state, payload) {
      state.actualJoke = payload
    },
  }
})
